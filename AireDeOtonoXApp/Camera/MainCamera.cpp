#include "MainCamera.h"

// Singleton
MainCamera *MainCamera::mainCameraInstance = nullptr;

MainCamera *MainCamera::GetInstance()
{
	if (mainCameraInstance == nullptr)
		mainCameraInstance = new MainCamera();

	return mainCameraInstance;
}


MainCamera::MainCamera() :
	posicion(0.f, 0.f, 0.f),
	cameraUp(0.f, 1.f, 0.f),
	perpendicular(0),
	viewDirection(0),
	lookAt(0),
	isViewHaveChanged(true),
	isProjHaveChanged(true),
	movementSpeed(60.0f),
	fov(MathUtil::Pi / 4),
	aspect(1.f),
	fNear(0.01f),
	fFar(800.0f),
	angleX(0),
	angleY(0),
	windowSize(0),
	mouseDelta(0),
	rotationSpeed(0.1),
	radX(0),
	mouseOrientation(0)
{ }

MainCamera::~MainCamera()
{ }

void MainCamera::Initialize(mVector<float> vPosition, mVector<float> vViewDirection)
{
	posicion = vPosition;
	lookAt = vViewDirection;
	viewDirection = lookAt - vPosition;
	viewDirection = viewDirection.getUnitario();
	isViewHaveChanged = true;
	mainCameraObject = this;
	perpendicular = mVector<float>::getProductoCruz(viewDirection, cameraUp);
}

void MainCamera::hasChanged()
{
	isViewHaveChanged = true;
}

void MainCamera::moveCameraForwardBackward(const int& iZoomDirection)
{
	if (iZoomDirection > 0)
		this->moveForward(0);
	else
		this->moveBackward(0);
}

void MainCamera::moveCameraTo(float deltaTime)
{
	viewDirection = lookAt - posicion;
	viewDirection = viewDirection.getUnitario();

	perpendicular = mVector<float>::getProductoCruz(viewDirection, cameraUp);
}

void MainCamera::moveForward(const float& deltaTime)
{
	posicion += viewDirection * movementSpeed * deltaTime;
	lookAt += viewDirection * movementSpeed* deltaTime;
}

void MainCamera::moveBackward(const float& deltaTime)
{
	posicion += viewDirection * -movementSpeed * deltaTime;
	lookAt += viewDirection * -movementSpeed * deltaTime;
}

void MainCamera::moveLeft(const float& deltaTime)
{
	posicion += perpendicular * -movementSpeed * deltaTime;
	lookAt += perpendicular * -movementSpeed * deltaTime;
}

void MainCamera::moveRight(const float& deltaTime)
{
	posicion += perpendicular * movementSpeed * deltaTime;
	lookAt += perpendicular * movementSpeed * deltaTime;
}

void MainCamera::moveUp(const float& deltaTime)
{
	posicion += cameraUp * movementSpeed * deltaTime;
	lookAt += cameraUp * movementSpeed * deltaTime;
}

void MainCamera::moveDown(const float& deltaTime)
{
	posicion += cameraUp * -movementSpeed * deltaTime;
	lookAt += cameraUp * -movementSpeed * deltaTime;
}

void MainCamera::testCameraRotation(float deltaTime)
{


}

void MainCamera::testCameraDirection(mVector<float>& vMouseDelta)
{

}

void MainCamera::rotateCamera(const mVector<float>& vPos, const float& deltaTime)
{
	/*m_fAngleY += (((vPos.y * 100) / m_vWindowSize.y) * 80) / 100;
	m_fAngleY += (sin(1.f) * m_fAngleY) + (cos(1.f) * m_fAngleY);*/
	float gradSum = (vPos.x * (rotationSpeed));
	angleX += gradSum;

	//m_fAngleX += (sin(1.f) * m_fAngleX) + (cos(1.f) * m_fAngleX);

	/*m_fAngleX += (((vPos.x * 100) / m_vWindowSize.x) * 180) / 100;
	m_fAngleX += (sin(1.f) * m_fAngleX) + (cos(1.f) * m_fAngleX);*/

	isViewHaveChanged = 1;
}

void MainCamera::setCameraPosition(const mVector<float>& vPosition)
{
	posicion = vPosition;
	isViewHaveChanged = 1;
}

void MainCamera::setCameraViewDirection(const mVector<float>& vViewDirection)
{
	viewDirection = vViewDirection;
	lookAt = vViewDirection;
	//m_vViewDirection = m_vViewDirection.getUnitario();
	isViewHaveChanged = 1;
}

mVector<float> MainCamera::getCameraViewDirection()
{
	viewDirection = lookAt - posicion;
	viewDirection = viewDirection.getUnitario();

	mVector<float> vDirector = mVector<float>(0, 0, 1);
	vDirector = mVector<float>::getDirector2D_XZ(angleX, vDirector);

	return vDirector;
}

MathUtil::CMatrix& MainCamera::GetView()
{
	if (!isViewHaveChanged)
		return matrizDeVista;

	MathUtil::sVector4f Eye = { { posicion.x, posicion.y, posicion.z, 0.0f } };
	MathUtil::sVector4f At = { { lookAt.x, lookAt.y, lookAt.z, 0.f } };
	MathUtil::sVector4f Up = { { cameraUp.x, cameraUp.y, cameraUp.z, 0.f } };
	matrizDeVista = MathUtil::MatrixLookAtLH(Eye, At, Up);
	radX = MathUtil::GradToRad(angleX);
	MathUtil::CMatrix mRotationX = MathUtil::MatrixRotationX(angleY);
	MathUtil::CMatrix mRotationY = MathUtil::MatrixRotationY(radX);
	MathUtil::CMatrix mRotation = MathUtil::Multiply(mRotationX, mRotationY);
	matrizDeVista = MathUtil::Multiply(mRotationY, matrizDeVista);
	isViewHaveChanged = false;

	return matrizDeVista;
}

MathUtil::CMatrix& MainCamera::GetProjection()
{
	if (!isProjHaveChanged)
		return matrizDeProyeccion;

	matrizDeProyeccion = MathUtil::MatrixPerspectiveFovLH(fov, aspect, fNear, fFar);
	isProjHaveChanged = false;

	return matrizDeProyeccion;
}


#pragma once
#include <d3d11.h>
#include <memory>
#include "../Recursos/UMath.h"
#include "../Recursos/mVector.h"

struct sConstantBuffer;

class MainCamera
{

public:
	~MainCamera();

	void Initialize(mVector<float> vPosition, mVector<float> vViewDirection);

	void update(float deltaTime);
	void release();

	static MainCamera *GetInstance();

	void hasChanged();

	MathUtil::CMatrix &GetView();
	MathUtil::CMatrix &GetProjection();

	void setCameraPosition(const mVector<float>& vPosition);
	void setCameraViewDirection(const mVector<float>& vViewDirection);

	mVector<float> getCameraViewDirection();

	mVector<float> getCameraPosition()		{ return posicion; }
	float getCameraSpeedMovement()			{ return movementSpeed; }
	float getCameraFov()					{ return fov; }
	float getCameraAspect()					{ return aspect; }
	float getCameraNear()					{ return fNear; }
	float getCameraFar()					{ return fFar; }

	void setCameraSpeedMovement(const float& fSpeed)		{ movementSpeed = fSpeed; }
	void setCameraFov(const float& fFov)					{ fov = fFov; }
	void setCameraAspect(const float& fAspect)				{ aspect = fAspect; }
	void setCameraNear(const float& nearToSet)				{ fNear = nearToSet; }
	void setCameraFar(const float& farToSet)				{ fFar = farToSet; }

	mVector<float> getWindowSize()	{ return windowSize; }
	float getWindowWidth()			{ return windowSize.x; }
	float getWindowHeight()			{ return windowSize.y; }

	bool testCameraMovement() { return isViewHaveChanged; }

	float getCameraGrados() { return angleX; }

	void moveForward(const float& deltaTime);
	void moveDown(const float& deltaTime);
	void moveBackward(const float& deltaTime);

private:
	MainCamera();

	void rotateCamera(const mVector<float>& vPos, const float& deltaTime);
	void testCameraDirection(mVector<float>& vMouseDelta);

private:

	void moveCameraForwardBackward(const int& iZoomDirection);
	void moveCameraTo(float deltaTime);
	void testCameraRotation(float deltaTime);



	void moveLeft(const float& deltaTime);
	void moveRight(const float& deltaTime);
	void moveUp(const float& deltaTime);



protected:

	MainCamera* mainCameraObject;

	mVector<float> posicion;
	mVector<float> windowSize;

	bool isViewHaveChanged;
	bool isProjHaveChanged;

	MathUtil::CMatrix matrizDeVista;
	MathUtil::CMatrix matrizDeProyeccion;

	mVector<float> viewDirection;
	mVector<float> lookAt;
	mVector<float> cameraUp;
	mVector<float> perpendicular;

	MathUtil::CMatrix cameraLookAt;

	float movementSpeed;
	float rotationSpeed;
	float fov;
	float aspect;
	float fNear;
	float fFar;

	float angleY;
	float angleX;
	float radX;

	int mouseOrientation;
	mVector<float> mouseDelta;

	static MainCamera *mainCameraInstance;
};
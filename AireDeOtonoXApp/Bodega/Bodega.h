//
//  Bodega.h
//
//
//  Created by Max on 25/09/16.
//

#pragma once
#include <map>

template< class T > class Bodega
{
public:
	Bodega();
	~Bodega();

	T getObjetoEnBodegaConLlave(char llave);
	void setObjetoEnBodegaConLlave(char llave, T*);
	
public:
	std::map<char, T> bodegaMap;
};

template< class T >
Bodega< T >::Bodega()
{ }

template< class T >
Bodega< T >::~Bodega()
{ }

template< class T > 
T Bodega< T >::getObjetoEnBodegaConLlave(char llave)
{
	return bodegaMap[llave];
}

template< class T >
void Bodega< T >::setObjetoEnBodegaConLlave(char llave, T*)
{
	//bodegaMap[llave] = T;
}
//
//  mVector.h
//
//
//  Created by Max on 11/17/14.
//

#pragma once

#include <iostream>
#include <sstream>
#include "math.h"

#define _Pi 3.14159265359

template< class T > class mVector
{

public:

	T x;
	T y;
	T z;

	mVector();
	mVector(const T& _x, const T& _y, const T& _z = 0);

	mVector(const T& alfa);

	mVector& operator = (const mVector& vec);
	mVector& operator = (const T& alfa);

	mVector& operator += (const T& alfa);
	mVector operator + (const T& alfa);

	mVector& operator -= (const T& alfa);
	mVector operator - (const T& alfa);

	mVector& operator += (const mVector& vec);
	mVector operator + (const mVector& vec);

	mVector operator - (const mVector& vec);
	mVector& operator -=(const mVector& vec);

	mVector& operator *= (const T& alfa);
	mVector operator * (const T& alfa);

	mVector& operator /= (const T& alfa);
	mVector operator / (const T& alfa);

	mVector& operator *= (const mVector& vec);
	mVector operator * (const mVector& vec);

	mVector& operator /= (const mVector& vec);
	mVector operator / (const mVector& vec);

	mVector getChangeSentido();
	mVector getUnitario();
	mVector productoCruz(const mVector< T >& vec1, const mVector< T >& vec2);

	static mVector getProductoCruz(const mVector< T >& vec1, const mVector< T >& vec2)
	{
		T _x = (vec1.y * vec2.z) - (vec2.y * vec1.z);
		T _y = (vec1.x * vec2.z) - (vec2.x * vec1.x);
		T _z = (vec1.x * vec2.y) - (vec2.x * vec1.y);

		return mVector(_x, -_y, _z);
	}

	static mVector distanciaEntrePuntos2D(const mVector< T >& vec1, const mVector< T >& vec2)
	{
		T _x = vec2.x - vec1.x;
		T _y = vec2.y - vec1.y;

		if (_x < 0) _x *= -1;
		if (_y < 0) _y *= -1;

		return mVector(_x, _y, 0);
	}

	static mVector getDirector2D_XZ(const T& theta, const mVector< T >& vec)
	{
		T _x = vec.x;
		T _z = vec.z;
		_x = sin(theta);
		_z = cos(theta);

		return mVector(_x, 0, _z);
	}

	T productoPuntoWith(const mVector& vec);
	T getMagnitud();
	T getAnguloEntreVectores(mVector vec);
	T getRadianesEntreVectores(const mVector& vec);
	T getGrados();
	T getGradosForZ();
	T getRadianes();

	void changeSentido();
	void getDirectorOfRotation(const T& theta);
	void getDirectorOfRotationInZ(const T& theta);
	void rotate(const T& theta);

	bool operator != (const mVector& vec);
	bool operator == (const mVector& vec);
	bool operator <= (const mVector& vec);
	bool operator >= (const mVector& vec);

	std::string toString();

	//friend std::ostream& operator << ( std::ostream& o, const mVector< T >& vec );
};


template< class T > mVector< T >::mVector() : x(0),
y(0),
z(0)
{ }

template< class T > mVector< T >::mVector(const T&  _x, const T& _y, const T& _z) : x(_x),
y(_y),
z(_z)
{ }

template< class T > mVector< T >::mVector(const T& alfa) : x(alfa), y(alfa), z(alfa)
{ }

template< class T > mVector< T >& mVector< T >::operator = (const mVector< T >& vec)
{
	if (&vec == this)
		return *this;

	this->x = vec.x;
	this->y = vec.y;
	this->z = vec.z;

	return *this;
}

template< class T > mVector< T >& mVector< T >::operator = (const T& alfa)
{
	this->x = alfa;
	this->y = alfa;
	this->z = alfa;

	return *this;
}

template< class T > mVector< T >& mVector< T >::operator += (const T& alfa)
{
	this->x += alfa;
	this->y += alfa;
	this->z += alfa;

	return *this;
}

template< class T > mVector< T > mVector< T >::operator + (const T& alfa)
{
	return mVector(this->x + alfa, this->y + alfa, this->z + alfa);
}

template< class T > mVector< T >& mVector< T >::operator -= (const T& alfa)
{
	this->x -= alfa;
	this->y -= alfa;
	this->z -= alfa;

	return *this;
}

template< class T > mVector< T > mVector< T >::operator - (const T& alfa)
{
	return mVector(this->x - alfa, this->y - alfa, this->z - alfa);
}

template< class T > mVector< T >& mVector< T >::operator += (const mVector< T >& vec)
{
	this->x += vec.x;
	this->y += vec.y;
	this->z += vec.z;

	return *this;
}

template< class T > mVector< T > mVector< T >::operator + (const mVector< T >& vec)
{
	return mVector(this->x + vec.x, this->y + vec.y, this->z + vec.z);
}

template< class T > mVector< T >& mVector< T >::operator -=(const mVector< T >& vec)
{
	this->x -= vec.x;
	this->y -= vec.y;
	this->z -= vec.z;

	return *this;
}

template< class T > mVector< T > mVector< T >::operator - (const mVector< T >& vec)
{
	return mVector(this->x - vec.x, this->y - vec.y, this->z - vec.z);
}

template< class T > mVector< T > mVector< T >::operator * (const T& alfa)
{
	return mVector(this->x * alfa, this->y * alfa, this->z * alfa);
}

template< class T > mVector< T >& mVector< T >::operator *= (const T& alfa)
{
	this->x *= alfa;
	this->y *= alfa;
	this->z *= alfa;

	return *this;
}

template< class T > mVector< T > mVector< T >::operator / (const T& alfa)
{
	return mVector(this->x / alfa, this->y / alfa, this->z / alfa);
}

template< class T > mVector< T >& mVector< T >::operator /= (const T& alfa)
{
	this->x /= alfa;
	this->y /= alfa;
	this->z /= alfa;

	return *this;
}

template< class T > mVector< T > mVector< T >::operator * (const mVector< T >& vec)
{
	return mVector(
		y * vec.z - vec.y * z,
		-x * vec.z - vec.x * z,
		x * vec.y - vec.x * y
		);
}

template< class T > mVector< T >& mVector< T >::operator *= (const mVector< T >& vec)
{
	this->x = this->y * vec.z - vec.y * this->z;
	this->y = -this->x * vec.z - vec.x * this->z;
	this->z = this->x * vec.y - vec.x * this->y;

	return *this;
}

template< class T > mVector< T > mVector< T >::operator / (const mVector< T >& vec)
{
	return mVector(this->x / vec.x, this->y / vec.y, this->z / vec.z);
}

template< class T > mVector< T >& mVector< T >::operator /= (const mVector< T >& vec)
{
	this->x /= vec.x;
	this->y /= vec.y;
	this->z /= vec.z;

	return *this;
}

template< class T > bool mVector< T >::operator != (const mVector< T >& vec)
{
	if (vec.x == this->x && vec.y == this->y && vec.z == this->z) return 0;
	return 1;
}

template< class T > bool mVector< T >::operator == (const mVector< T >& vec)
{
	if (vec.x != this->x || vec.y != this->y || vec.z != this->z) return 0;
	return 1;
}

template< class T > bool mVector< T >::operator <= (const mVector< T >& vec)
{
	if (this->x <= vec.x && this->y <= vec.y && this->z <= vec.z) return 1;
	return 0;
}

template< class T > bool mVector< T >::operator >= (const mVector< T >& vec)
{
	if (this->x >= vec.x && this->y >= vec.y && this->z >= vec.z) return 1;
	return 0;
}

template< class T > std::string mVector< T >::toString()
{
	std::stringstream ssStream;
	ssStream << "x = " << x << "\n" << "y = " << y << "\n" << "z = " << z << "\n";
	return std::string(ssStream.str());
}

template< class T > T mVector< T >::getMagnitud()
{
	T _x = x * x;
	T _y = y * y;
	T _z = z * z;
	T _mag = sqrt(_x + _y + _z);

	return _mag;
}

template< class T > T mVector< T >::productoPuntoWith(const mVector< T >& vec)
{
	T _a = x * vec.x;
	T _b = y * vec.y;
	T _z = z * vec.z;

	return  _a + _b + _z;
}

template< class T > T mVector< T >::getRadianesEntreVectores(const mVector< T >& vec)
{
	T _a = getMagnitud();
	T _b = vec.getMagnitud();

	T _ab = productoPuntoWith(vec);

	_ab = _ab / (_a * _b);

	T _ang = acos(_ab) * 180.0 / _Pi;

	return _ang;
}

template< class T > T mVector< T >::getAnguloEntreVectores(mVector< T > vec)
{
	T _a = getMagnitud();
	T _b = vec.getMagnitud();

	T _ab = productoPuntoWith(vec);

	_ab = _ab / (_a * _b);

	T _ang = acos(_ab) * 180.0 / _Pi;

	return _ang;
}

template< class T > mVector< T > mVector< T >::getChangeSentido()
{
	return mVector(this->x * -1, this->y * -1, this->z * -1);
}

template< class T > mVector< T > mVector< T >::getUnitario()
{
	return mVector(this->x / this->getMagnitud(), this->y / this->getMagnitud(), this->z / this->getMagnitud());
}

template< class T > mVector< T > mVector< T >::productoCruz(const mVector< T >& vec1, const mVector< T >& vec2)
{
	T _x = (vec1.y * vec2.z) - (vec2.y * vec1.z);
	T _y = (vec1.x * vec2.z) - (vec2.x * vec1.x);
	T _z = (vec1.x * vec2.y) - (vec2.x * vec1.y);

	return mVector(_x, -_y, _z);
}

template< class T > void mVector< T >::changeSentido()
{
	this->x *= -1;
	this->y *= -1;
	this->z *= -1;
}

template< class T > void mVector< T >::getDirectorOfRotation(const T& theta)
{
	T _theta = this->getGrados();

	_theta += theta;

	return mVector(this->x = cos(_theta), this->y = sin(_theta));
}

template< class T > void mVector< T >::getDirectorOfRotationInZ(const T& theta)
{
	T _theta = this->getGradosForZ();

	_theta += theta;
	this->x = cos(_theta);
	this->z = sin(_theta);
}

template< class T > void mVector< T >::rotate(const T& theta)
{
	T _theta = this->getGrados();

	_theta += theta;

	this->x = cos(_theta);
	this->y = sin(_theta);
}

template< class T > T mVector< T >::getGrados()
{
	return atan2(this->y, this->x) * 180 / _Pi;
}

template< class T > T mVector< T >::getGradosForZ()
{
	return atan2(this->x, this->z) * 180 / _Pi;
}

template< class T > T mVector< T >::getRadianes()
{
	return atan(this->y / this->x);
}


/*
////Friend ostream sobrecarga
std::ostream& operator << ( std::ostream& o, const mVector< class T >& vec )
{
std::cout << "( x::" << vec.x << "  ,   y::" << vec.y << ")" << std::endl;
}
*/



//
//	Base para todos los modelos de directX 
//
//	Creado por Maximiliano Oviedo
//	28/01/2016
//

#pragma once
//#include "../Shader/ShaderCreator.h"
#include "../Shader/ShaderStructures.h"
#include "../Shaders/Factory/ShaderFactory.h"

class ModelEntity
{

public:

	virtual ~ModelEntity();

	void Initialize(ID3D11Device *device, ID3D11DeviceContext *context);

	virtual void createModelGeometry() = 0;
	virtual void render() = 0;
	virtual void update(const float& deltaTime) = 0;

	ID3D11VertexShader* sVertexShader;
	ID3D11PixelShader* pixelShader;

	ID3D11Buffer *GetVertices()			{ return vertexBuffer; }
	ID3D11Buffer *GetColors()			{ return pixelBuffer; }
	ID3D11Buffer *GetIndices()			{ return indexBuffer; }
	unsigned int GetIndexBufferSize()	{ return indexBufferSize; }
	int getCountOfVertices()			{ return numVertices; }
	int getCountOfIndices()				{ return numIndexes; }
	void setAppBufferToUpdate()			{ isAppBufferUpdating = true; }

	void setColor(float r, float g, float b, float a);
	void sumColor(float r, float g, float b, float a);

	virtual void release();
	void setBuffers();

	bool setDevice();

protected:

	ModelEntity();

	virtual void drawModelVertexPerVertex();
	virtual void drawModelIndexed();

	void setVertexBuffer();
	void setPixelBuffer();
	void activateBuffers();
	void activateForLinesBuffers();
	void setRenderProperties();

	void setAppBuffer();
	void setFrameBuffer();
	void setObjectBuffer();
	void setColorBuffer();

protected:

	std::vector<BasicTexVertex> modelVerticesWithColor;
	std::vector<unsigned short> verticesIndex;

	ID3D11Device		*DXdevice;
	ID3D11DeviceContext *DXcontext;

	ID3D11Buffer		*vertexBuffer;
	ID3D11Buffer		*pixelBuffer;
	ID3D11Buffer		*indexBuffer;

	ID3D11SamplerState				*mpSamplerLinear;

	MathUtil::sVector4f colorVectorWithAlpha;

	unsigned int indexBufferSize;
	unsigned int numVertices;
	unsigned int numIndexes;

	bool isAppBufferUpdating;
	bool isModelBufferUpdating;
	bool isColorBufferUpdating;
	bool isDrawingIndexed;


	ID3D11ShaderResourceView* textura;
};



//
//	Grid 
//
//	Creado por Maximiliano Oviedo
//	
//

#pragma once
#include "ModelEntity.h"

class SquareGeometry : public ModelEntity
{

public:
	SquareGeometry();
	~SquareGeometry();

	static SquareGeometry *GetInstance();

	void InitTexture(ID3D11Device *device, ID3D11DeviceContext *context);

	void render();
	void createModelGeometry();
	void update(const float& deltaTime);

	void release() override;

private:


};
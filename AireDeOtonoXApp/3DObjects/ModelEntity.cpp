
#include "ModelEntity.h"
#include "../Camera/MainCamera.h"
#include "../Recursos/mVector.h"
#include "../Shaders/Arrange/ShadersArrange.h"
#include "../Shaders/Shaders/Vertex/DefaultVertexShader.h"

ModelEntity::ModelEntity() : DXdevice(nullptr),
DXcontext(nullptr),
indexBuffer(nullptr),
pixelBuffer(nullptr),
vertexBuffer(nullptr),
indexBufferSize(0),
isAppBufferUpdating(1),
isModelBufferUpdating(1),
isColorBufferUpdating(1),
isDrawingIndexed(false)
{
	colorVectorWithAlpha = MathUtil::GetColorWithRGB(.3f, .7f, .7f);
}

ModelEntity::~ModelEntity()
{ }

void ModelEntity::Initialize(ID3D11Device *device, ID3D11DeviceContext *context)
{
	DXdevice = device;
	DXcontext = context;

	ShadersArrange* shadersArrange = new ShadersArrange();
	DefaultVertexShader* defaultShader = new DefaultVertexShader();
	shadersArrange->arrangeVertexShaderClass(defaultShader, device);

	DefaultVertexShader* defShader = new DefaultVertexShader();
	shadersArrange->arrangeVertexShaderClass(defShader, device);
	delete defaultShader;

	//sVertexShader = shaderFactory.getVertexShader(device);
	//pixelShader = shaderFactory.getPixelShader(device);

	//shaderCreator.Initialize(pixelShaderType, vertexLayoutDesc, _countof(vertexLayoutDesc), device);
}

void ModelEntity::setBuffers()
{
	setVertexBuffer();
}

void ModelEntity::setVertexBuffer()
{
	if (DXdevice == nullptr)
		return;

	//indexBufferSize = verticesIndex.size();

	D3D11_BUFFER_DESC vertexBufferDesc = { 0 };
	vertexBufferDesc.ByteWidth = numVertices; //sizeof(BasicVertexColor) * modelVerticesWithColor.size();
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA vertexBufferData;
	vertexBufferData.pSysMem = &modelVerticesWithColor[0];
	vertexBufferData.SysMemPitch = 0;
	vertexBufferData.SysMemSlicePitch = 0;

	DXdevice->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &vertexBuffer);

	if (verticesIndex.size() > 0)
	{
		isDrawingIndexed = true;
		D3D11_BUFFER_DESC indexBufferDesc;
		ZeroMemory(&indexBufferDesc, sizeof(D3D11_BUFFER_DESC));
		indexBufferDesc.ByteWidth = indexBufferSize;
		indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
		indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		indexBufferDesc.CPUAccessFlags = 0;
		indexBufferDesc.MiscFlags = 0;
		indexBufferDesc.StructureByteStride = 0;

		D3D11_SUBRESOURCE_DATA indexBufferData;
		indexBufferData.pSysMem = &verticesIndex[0];
		indexBufferData.SysMemPitch = 0;
		indexBufferData.SysMemSlicePitch = 0;

		DXdevice->CreateBuffer(&indexBufferDesc, &indexBufferData, &indexBuffer);
	}

}

void ModelEntity::setPixelBuffer()
{
	if (DXdevice == nullptr)
		return;

	D3D11_BUFFER_DESC pixelBufferDesc = { 0 };
	//	pixelBufferDesc.ByteWidth = sizeof(BasicPosVertex) * m_vColorVertices.size();
	pixelBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	pixelBufferDesc.BindFlags = D3D11_BLEND_DEST_COLOR;
	pixelBufferDesc.CPUAccessFlags = 0;
	pixelBufferDesc.MiscFlags = 0;
	pixelBufferDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA pixelBufferData;
	//pixelBufferData.pSysMem = &m_vColorVertices[0];
	pixelBufferData.SysMemPitch = 0;
	pixelBufferData.SysMemSlicePitch = 0;

	//m_pDevice->CreateBuffer(&pixelBufferDesc, &pixelBufferData, &m_pPixelBuffer);
}

void ModelEntity::drawModelVertexPerVertex()
{
	setRenderProperties();
	activateBuffers();
	DXcontext->Draw(numVertices, 0);
}

void ModelEntity::drawModelIndexed()
{
	setRenderProperties();
	activateBuffers();
	DXcontext->DrawIndexed(numIndexes, 0, 0);
}

void ModelEntity::setRenderProperties()
{
	ID3D11VertexShader *vshader = sVertexShader;
	ID3D11PixelShader *pshader = pixelShader;

	DXcontext->VSSetShader(vshader, nullptr, 0);
	DXcontext->PSSetShader(pshader, nullptr, 0);

	//Revisar lo de los registros
	if (isAppBufferUpdating && !(isAppBufferUpdating = 0)) setAppBuffer();
	setFrameBuffer();
	if (isModelBufferUpdating && !(isModelBufferUpdating = 0)) setObjectBuffer();
	if (isColorBufferUpdating && !(isColorBufferUpdating = 0)) setColorBuffer();

	/*ID3D11Buffer **constantBuffers = shaderCreator.GetConstantBuffers();
	DXcontext->VSSetConstantBuffers(0, NumShadersBuffers, constantBuffers);

	ID3D11Buffer* colorConstantBuffer = shaderCreator.GetColorConstantBuffer();
	DXcontext->PSSetConstantBuffers(0, 1, &colorConstantBuffer);

	ID3D11InputLayout *inlay = shaderCreator.GetInputLayout();
	DXcontext->IASetInputLayout(inlay);*/
}

void ModelEntity::setAppBuffer()
{
	/*ID3D11Buffer *appBuffer = shaderCreator.GetAppConstantBuffer();
	ShaderCBuffer &bufProjectionData = shaderCreator.GetProjectionBuffer();

	MathUtil::CMatrix mProjection = MainCamera::GetInstance()->GetProjection();
	bufProjectionData.cBuffer = MathUtil::MatrixTranspose(mProjection);

	DXcontext->UpdateSubresource(shaderCreator.GetAppConstantBuffer(), 0, nullptr, &bufProjectionData, 0, 0);*/
}

void ModelEntity::setFrameBuffer()
{
	/*ID3D11Buffer *frameBuffer = shaderCreator.GetFrameConstantBuffer();
	ShaderCBuffer &bufViewData = shaderCreator.GetViewBuffer();

	MathUtil::CMatrix mView = MainCamera::GetInstance()->GetView();
	bufViewData.cBuffer = MathUtil::MatrixTranspose(mView);

	DXcontext->UpdateSubresource(shaderCreator.GetFrameConstantBuffer(), 0, nullptr, &bufViewData, 0, 0);*/
}

void ModelEntity::setObjectBuffer()
{
	/*ID3D11Buffer *objectBuffer = shaderCreator.GetFrameConstantBuffer();
	ShaderCBuffer &bufModelData = shaderCreator.GetModelBuffer();

	bufModelData.cBuffer = MathUtil::MatrixIdentity();

	DXcontext->UpdateSubresource(shaderCreator.GetObjectConstantBuffer(), 0, nullptr, &bufModelData, 0, 0);*/
}

void ModelEntity::setColorBuffer()
{
	/*sPSColor &bufColorData = shaderCreator.GetColorBuffer();

	bufColorData.color = colorVectorWithAlpha;

	DXcontext->UpdateSubresource(shaderCreator.GetColorConstantBuffer(), 0, nullptr, &bufColorData, 0, 0);*/
}

void ModelEntity::setColor(float r, float g, float b, float a)
{
	colorVectorWithAlpha.f[0] = r;
	colorVectorWithAlpha.f[1] = g;
	colorVectorWithAlpha.f[2] = b;
	colorVectorWithAlpha.f[3] = b;

	isColorBufferUpdating = true;
}

void ModelEntity::sumColor(float r, float g, float b, float a)
{
	colorVectorWithAlpha.f[0] += r;
	colorVectorWithAlpha.f[1] += g;
	colorVectorWithAlpha.f[2] += b;
	colorVectorWithAlpha.f[3] += a;

	isColorBufferUpdating = true;
}

void ModelEntity::activateBuffers()
{
	unsigned int stride = sizeof(BasicTexVertex);
	unsigned int offset = 0;

	ID3D11ShaderResourceView *sres = textura;
	DXcontext->PSSetShaderResources(0, 1, &sres);

	ID3D11SamplerState *sste = mpSamplerLinear;
	DXcontext->PSSetSamplers(0, 1, &sste);

	DXcontext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	DXcontext->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
	DXcontext->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R16_UINT, 0);
}

void ModelEntity::activateForLinesBuffers()
{
	unsigned int stride = sizeof(BasicTexVertex);
	unsigned int offset = 0;
	DXcontext->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
	DXcontext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
}

bool ModelEntity::setDevice()
{

	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;//D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;//D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;//D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;

	HRESULT hr = DXdevice->CreateSamplerState(&sampDesc, &mpSamplerLinear);
	if (FAILED(hr))
		return false;

	D3D11_BLEND_DESC BlendStateDescription;
	ID3D11BlendState* pBlendState = NULL;
	ZeroMemory(&BlendStateDescription, sizeof(D3D11_BLEND_DESC));

	BlendStateDescription.RenderTarget[0].BlendEnable = TRUE;
	BlendStateDescription.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

	BlendStateDescription.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	BlendStateDescription.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	BlendStateDescription.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;

	BlendStateDescription.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	BlendStateDescription.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	BlendStateDescription.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;

	hr = DXdevice->CreateBlendState(&BlendStateDescription, &pBlendState);
	float blendFactor[] = { 0, 0, 0, 0 };
	UINT sampleMask = 0xffffffff;

	DXcontext->OMSetBlendState(pBlendState, blendFactor, sampleMask);

	return true;
}

void ModelEntity::release()
{
	if (vertexBuffer != nullptr)
	{
		vertexBuffer->Release();
		vertexBuffer = nullptr;
	}

	if (pixelBuffer != nullptr)
	{
		pixelBuffer->Release();
		pixelBuffer = nullptr;
	}

	if (indexBuffer != nullptr)
	{
		indexBuffer->Release();
		indexBuffer = nullptr;
	}
}

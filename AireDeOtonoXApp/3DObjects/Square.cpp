

#include "Square.h"
#include <d3d11.h>
#include "../Direct3DX_11/WICTextureLoader.h"

#include <string>
#include "../Shaders/Factory/ShaderFactory.h"

static SquareGeometry *instance = nullptr;

SquareGeometry::SquareGeometry() : ModelEntity()
{
	textura = nullptr;
	
}

SquareGeometry::~SquareGeometry()
{ }

SquareGeometry* SquareGeometry::GetInstance()
{
	if (instance == nullptr)
		instance = new SquareGeometry();

	return instance;
}

void SquareGeometry::InitTexture(ID3D11Device *device, ID3D11DeviceContext *context)
{

	ID3D11ShaderResourceView *pTexture;

	std::wstring dataPath(L"data/osostex.png");
	/*dataPath += filename;*/
	HRESULT hr = DirectX::CreateWICTextureFromFile(
		device, context, dataPath.c_str(), nullptr, &pTexture);

	textura = pTexture;
}

void SquareGeometry::createModelGeometry()
{
	if (DXdevice == nullptr)
		return;

	/*
	modelVerticesWithColor.push_back({ { 20.0f, 20.0f, 0.0f }, { 1.0f, 0.0f, 0.0f } });
	modelVerticesWithColor.push_back({ { 20.0f, -20.0f, 0.0f }, { 1.0f, 1.0f, 0.0f } });
	modelVerticesWithColor.push_back({ { -20.0f, -20.0f, 0.0f }, { 0.0f, 1.0f, 0.0f } });
	*/

	/*
	modelVerticesWithColor.push_back({ { -20.0f, 20.0f, 0.0f }, { 0.0f, 0.0f, 0.0f } });
	modelVerticesWithColor.push_back({ { 20.0f, 20.0f, 0.0f }, { 1.0f, 0.0f, 0.0f } });
	modelVerticesWithColor.push_back({ { -20.0f, -20.0f, 0.0f }, { 0.0f, 1.0f, 0.0f } });
	numVertices = modelVerticesWithColor.size() * sizeof(BasicVertexColor);
	*/
	const BasicTexVertex cubeVertices[] =
	{
		{ { -20.0f, 20.0f, 0.0f }, { 0.0f, 0.0f } },
		{ { 20.0f, 20.0f, 0.0f }, { 1.0f, 0.0f } },
		{ { -20.0f, -20.0f, 0.0f }, { 0.0f, 1.0f } },
		{ { 20.0f, -20.0f, 0.0f }, { 1.0f, 1.0f } }
		//{ XMFLOAT3(0.5f + x, -0.5f + y, -0.5f + z), XMFLOAT2(r, g) },
		//{ XMFLOAT3(0.5f + x, -0.5f + y,  0.5f + z), XMFLOAT2(r, g) },
		//{ XMFLOAT3(0.5f + x,  0.5f + y, -0.5f + z), XMFLOAT2(r, g) },
		//{ XMFLOAT3(0.5f + x,  0.5f + y,  0.5f + z), XMFLOAT2(r, g) },
	};

	unsigned int dataArraySize = sizeof(cubeVertices) / sizeof(BasicTexVertex);

	unsigned short lastIndex = static_cast<unsigned short>(modelVerticesWithColor.size());
	modelVerticesWithColor.insert(modelVerticesWithColor.end(), &cubeVertices[0], &cubeVertices[dataArraySize]);

	numVertices = modelVerticesWithColor.size() * sizeof(BasicTexVertex);

	unsigned short cubeIndices[] =
	{
		0, 1, 2,
		1, 3, 2
	};

	unsigned int cubeArraySize = sizeof(cubeIndices) / sizeof(unsigned short);
	verticesIndex.insert(verticesIndex.end(), &cubeIndices[0], &cubeIndices[cubeArraySize]);
	numIndexes = verticesIndex.size();
	indexBufferSize = numIndexes * sizeof(unsigned short);

	setBuffers();
}

void SquareGeometry::render()
{
	if (isDrawingIndexed)
		this->drawModelIndexed();
	else
		this->drawModelVertexPerVertex();
}

void SquareGeometry::update(const float& fDeltaTime)
{
	//sumColor(0.001f, 0.0001f, 0.0001f, 0.0f);
}

void SquareGeometry::release()
{
	SquareGeometry::release();
	delete instance;
}
#include "Direct3DX.h"
#include <d3d11.h>

Direct3DXCore::Direct3DXCore() :direct3DXDevice(0),
direct3DXContext(0),
direct3DXSwapChain(0),
direct3DXTargetView(0),
direct3DXDepthStencilView(0),
direct3DXDepthStencil(0)
{ }

Direct3DXCore::~Direct3DXCore()
{ }

HRESULT Direct3DXCore::Initialize(HWND &windowhandler, unsigned int width, unsigned int height)
{
	if (FAILED(InitializeDirect3DXContext(windowhandler, width, height))) return S_FALSE;

	if (FAILED(Initialize3DEnvironment(width, height))) return S_FALSE;

	return S_OK;
}

HRESULT Direct3DXCore::InitializeDirect3DXContext(HWND &windowhandler, unsigned int width, unsigned int height)
{
	HRESULT hResult = S_OK;

	DXGI_SWAP_CHAIN_DESC sd;
	ZeroMemory(&sd, sizeof(sd));

	sd.BufferCount = 1;
	sd.BufferDesc.Width = width;
	sd.BufferDesc.Height = height;
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	sd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	sd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	sd.BufferDesc.RefreshRate.Numerator = 60;
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;

	sd.OutputWindow = windowhandler;
	sd.SampleDesc.Count = 1;
	sd.SampleDesc.Quality = 0;

	sd.Windowed = TRUE;
	sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

	UINT createDeviceFlags = 0;

#ifdef _DEBUG
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	D3D_FEATURE_LEVEL featureLevel;
	hResult = D3D11CreateDeviceAndSwapChain(
		0, D3D_DRIVER_TYPE_HARDWARE, 0,
		createDeviceFlags, 0, 0,
		D3D11_SDK_VERSION,
		&sd, &direct3DXSwapChain,
		&direct3DXDevice,
		&featureLevel,
		&direct3DXContext);

	if (FAILED(hResult)) return S_FALSE; //No device

	if (featureLevel != D3D_FEATURE_LEVEL_11_1) return S_FALSE; //No DirectX11

	return hResult;
}

HRESULT Direct3DXCore::Initialize3DEnvironment(unsigned int width, unsigned int height)
{
	HRESULT hResult = S_OK;

	ID3D11Texture2D *pBackBuffer;
	hResult = direct3DXSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&pBackBuffer));
	if (FAILED(hResult)) return hResult;

	hResult = direct3DXDevice->CreateRenderTargetView(pBackBuffer, 0, &direct3DXTargetView);

	SafeRelease(pBackBuffer);

	if (FAILED(hResult)) return hResult;

	// Crea la textura del depth buffer
	D3D11_TEXTURE2D_DESC descDepth;
	descDepth.Width = width;
	descDepth.Height = height;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	descDepth.SampleDesc.Count = 1;
	descDepth.SampleDesc.Quality = 0;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	descDepth.CPUAccessFlags = 0;
	descDepth.MiscFlags = 0;

	hResult = direct3DXDevice->CreateTexture2D(&descDepth, 0, &direct3DXDepthStencil);
	if (FAILED(hResult)) { return hResult; }

	// Crea la "view" del depth buffer.
	hResult = direct3DXDevice->CreateDepthStencilView(direct3DXDepthStencil, 0, &direct3DXDepthStencilView);
	if (FAILED(hResult)) { return hResult; }

	direct3DXContext->OMSetRenderTargets(1, &direct3DXTargetView, direct3DXDepthStencilView);

	// Crea el viewport
	D3D11_VIEWPORT vp;
	vp.Width = static_cast<float>(width);
	vp.Height = static_cast<float>(height);
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	direct3DXContext->RSSetViewports(1, &vp);


	return hResult;
}

void Direct3DXCore::clearColor(const float *clearColor)
{
	direct3DXContext->ClearRenderTargetView(direct3DXTargetView, clearColor);

	direct3DXContext->ClearDepthStencilView(direct3DXDepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
}

void Direct3DXCore::Present()
{
	direct3DXSwapChain->Present(0, 0);
}

HRESULT Direct3DXCore::Close()
{
	SafeRelease(direct3DXTargetView);
	SafeRelease(direct3DXDepthStencil);
	SafeRelease(direct3DXDepthStencilView);
	SafeRelease(direct3DXSwapChain);
	SafeRelease(direct3DXContext);
	SafeRelease(direct3DXDevice);

	return S_OK;
}

#pragma once
#define DllImport   __declspec( dllimport )

#include <d3dcompiler.h>

#include "../Recursos/SafeRelease.h"

struct ID3D11Device;
struct IDXGISwapChain;
struct ID3D11DeviceContext;
struct ID3D11RenderTargetView;
struct ID3D11Texture2D;
struct ID3D11DepthStencilView;

class  Direct3DXCore
{

public:
	Direct3DXCore(void);
	virtual ~Direct3DXCore(void);


	HRESULT Initialize(HWND &windowhandler, unsigned int width, unsigned int height);

	HRESULT Close();

	ID3D11Device *Get3DDevice()						{ return direct3DXDevice; }
	ID3D11DeviceContext *GetDeviceContext()			{ return direct3DXContext; }
	IDXGISwapChain *GetSwapChain()					{ return direct3DXSwapChain; }

	ID3D11RenderTargetView *GetRenderTargetView()	{ return direct3DXTargetView; }
	ID3D11DepthStencilView *GetDepthStencilView()	{ return direct3DXDepthStencilView; }

	void clearColor(const float *clearColor);

	void Present();

private:
	HRESULT InitializeDirect3DXContext(HWND &windowhandler, unsigned int width, unsigned int height);
	HRESULT Initialize3DEnvironment(unsigned int width, unsigned int height);

private:

	ID3D11Device				*direct3DXDevice;
	ID3D11DeviceContext			*direct3DXContext;
	IDXGISwapChain				*direct3DXSwapChain;

	ID3D11RenderTargetView		*direct3DXTargetView;
	ID3D11DepthStencilView		*direct3DXDepthStencilView;

	ID3D11Texture2D				*direct3DXDepthStencil;

};
//
//	App Timer 
//	Creado por MAXIMILIANO OVIEDO ROSAS 
//	09/02/16
//

#pragma once

#include <windows.h>
#include <cmath>

const unsigned long MAX_SAMPLE_COUNT = 50;

class AppTime
{

public:
	AppTime();
	~AppTime();

	void timeInstant();
	float getDeltaTime() const;

	void Init();

private:
	float           timeScale;					// Amount to scale counter
	float           deltaTime;					// Time elapsed since previous frame

	__int64         currentTime;				// Current Performance Counter
	__int64         lastTime;					// Performance Counter last frame
	__int64         countsPerSecond;			// Performance Frequency

	float           frameTimer[MAX_SAMPLE_COUNT];

	unsigned long   sampleCount;
	unsigned long   frameRate;
};


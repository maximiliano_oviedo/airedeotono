
//
//	Programa para la Dise�o y Programaci�n de Videojuegos II
//	SAE Institue
//	Creado por Maximiliano Oviedo Rosas
//	el dia 21 de enero del 2016
//
//

#pragma once
#include <windows.h>

#define WIN32_LEAN_AND_MEAN

class WinApplication
{
public:

	WinApplication(void);
	virtual ~WinApplication(void);

	HINSTANCE &GetAppInstance()				{ return appInstance; }
	HWND &GetAppHwnd()						{ return appWindow; }

protected:

	virtual void GetWindowDims(unsigned int &width, unsigned int &height)		{ width = 800; height = 800; }
	virtual LPCSTR GetWinTitle()												{ return LPCSTR("DirectX11 App"); }

	virtual bool AppPreBegin()		{ return true; }
	virtual bool AppBegin()			{ return true; }
	virtual bool AppUpdate()		{ return true; }
	virtual bool AppRender()		{ return true; }
	virtual bool AppEnd()			{ return true; }
	virtual int WindowMainLoop()	{ return true; }

	virtual bool Paint(HWND AppHwnd, WPARAM Wparam, LPARAM Lparam)			{ return false; }
	virtual bool SysCommand(HWND AppHwnd, WPARAM Wparam, LPARAM Lparam);
	virtual bool ProcessMessage(UINT msg, WPARAM Wparam, LPARAM Lparam)		{ return false; }

	void CloseWindow();

private:

	bool InitializeWindow();
	int InitWindowApp(HINSTANCE hInstance);

	friend int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int);
	static LRESULT CALLBACK WndProc(HWND Hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

	HRESULT InitWindow();

protected:
	const unsigned int	WIDTH = 800;
	const unsigned int	HEIGHT = 600;

private:

	HWND				appWindow;
	HINSTANCE			appInstance;
	static WinApplication	*pointerToApp;
	const TCHAR			*CLASSNAME;
};



#include <WindowsApp.h>
#include <cstdio>
#include <fcntl.h>
#include <io.h>
#include <assert.h>

WinApplication *WinApplication::pointerToApp = 0;


WinApplication::WinApplication(void) : CLASSNAME(TEXT("DXSCRIPT"))
{
	assert(pointerToApp == nullptr);
	pointerToApp = this;
	appInstance = nullptr;
	appWindow = nullptr;

}

WinApplication::~WinApplication(void)
{
	pointerToApp = nullptr;
	appInstance = nullptr;
	appWindow = nullptr;
}

int WinApplication::InitWindowApp(HINSTANCE hInstance)
{
	appInstance = hInstance;

	if (!InitializeWindow())
		return 0;

	if (!AppBegin())
		return 0;

	return	WindowMainLoop();
}

bool WinApplication::InitializeWindow()
{
	if (FAILED(InitWindow()))
	{
		MessageBox(0, LPCSTR("Inicialización de Ventana - Fallida"),
			LPCSTR("Error Fatal"), MB_OK);
		return 0;
	}

	return 1;
}

HRESULT WinApplication::InitWindow()
{
	WNDCLASSEX wndClass = { 0 };
	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = appInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = CLASSNAME;
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	if (!RegisterClassEx(&wndClass))
		return E_FAIL;

	DWORD		dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
	DWORD		dwStyle = WS_OVERLAPPEDWINDOW;

	RECT windowRect = { 0, 0, (long)WIDTH, (long)HEIGHT };
	AdjustWindowRectEx(&windowRect, dwStyle, FALSE, dwExStyle);

	appWindow = CreateWindowEx(dwExStyle, CLASSNAME, GetWinTitle(),
		dwStyle | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
		CW_USEDEFAULT, CW_USEDEFAULT,
		windowRect.right - windowRect.left,
		windowRect.bottom - windowRect.top,
		NULL, NULL, appInstance, NULL);

	if (!appWindow)
		return E_FAIL;

	ShowWindow(appWindow, SW_SHOW);
	SetForegroundWindow(appWindow);
	UpdateWindow(appWindow);
	SetFocus(appWindow);

	return S_OK;
}


bool WinApplication::SysCommand(HWND AppHwnd, WPARAM Wparam, LPARAM Lparam)
{
	switch (Wparam)
	{
	case SC_SCREENSAVE:
	case SC_MONITORPOWER:
		return true;
	}

	return false;
}

void WinApplication::CloseWindow()
{
	if (appWindow)
	{
		DestroyWindow(appWindow);
		appWindow = nullptr;
	}
	if (appInstance)
	{
		UnregisterClass(CLASSNAME, appInstance);
		appInstance = 0;
	}
}

LRESULT CALLBACK WinApplication::WndProc(HWND Hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_CREATE:
	{
		if (!WinApplication::pointerToApp->ProcessMessage(msg, wParam, lParam))
			return DefWindowProc(Hwnd, msg, wParam, lParam);
		break;
	}
	case WM_PAINT:
	{
		if (!WinApplication::pointerToApp->Paint(Hwnd, wParam, lParam))
			if (!WinApplication::pointerToApp->ProcessMessage(msg, wParam, lParam))
				return DefWindowProc(Hwnd, msg, wParam, lParam);
		break;
	}
	case WM_SYSCOMMAND:
	{
		if (!WinApplication::pointerToApp->SysCommand(Hwnd, wParam, lParam))
			if (!WinApplication::pointerToApp->ProcessMessage(msg, wParam, lParam))
				return DefWindowProc(Hwnd, msg, wParam, lParam);
		break;
	}

	case WM_DESTROY:
	{
		if (!WinApplication::pointerToApp->ProcessMessage(msg, wParam, lParam))
			PostQuitMessage(0);
		break;
	}
	default:
	{
		if (!WinApplication::pointerToApp->ProcessMessage(msg, wParam, lParam))
			return DefWindowProc(Hwnd, msg, wParam, lParam);
	}
	}
	return 0;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR t, int nShowCmd)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(nShowCmd);

	assert(WinApplication::pointerToApp);
	return WinApplication::pointerToApp->InitWindowApp(hInstance);
}




#include "AppTime.h"

AppTime::AppTime() : deltaTime(0),
frameRate(0),
currentTime(0),
sampleCount(0)
{ }

AppTime::~AppTime()
{
	deltaTime = 0;
	frameRate = 0;
	sampleCount = 0;

	currentTime = 0;
	lastTime = 0;
	countsPerSecond = 0;
}

void AppTime::Init()
{
	QueryPerformanceFrequency((LARGE_INTEGER *)&countsPerSecond);
	QueryPerformanceCounter((LARGE_INTEGER *)&lastTime);
	timeScale = 1.0f / countsPerSecond;
}

void AppTime::timeInstant()
{
	/*
	QueryPerformanceCounter((LARGE_INTEGER *)&m_iCurrentTime);

	float fDeltaTime = (m_iCurrentTime - m_iLastTime) * m_fTimeScale;

	m_iLastTime = m_iCurrentTime;
	m_fDeltaTime = fDeltaTime;

	if (fabsf(fDeltaTime - m_fDeltaTime) < 1.0f)
	{
	memmove(&m_fFrameTime[1], m_fFrameTime, (MAX_SAMPLE_COUNT - 1) * sizeof(float));
	m_fFrameTime[0] = fDeltaTime;
	if (m_ulSampleCount < MAX_SAMPLE_COUNT) m_ulSampleCount++;
	}

	m_fDeltaTime = 0.0f;

	if (m_ulSampleCount > 0)
	{
	for (ULONG i = 0; i < m_ulSampleCount; i++) m_fDeltaTime += m_fFrameTime[i];
	m_fDeltaTime /= m_ulSampleCount;
	}
	*/

	__int64 currTime;
	QueryPerformanceCounter((LARGE_INTEGER*)&currTime);

	currentTime = static_cast<int>(currTime);
	// Time difference between this frame and the previous.

	deltaTime = (currentTime - lastTime) * timeScale;

	// Prepare for next frame.
	lastTime = static_cast<int>(currentTime);

	if (deltaTime< 0.0f)
		deltaTime = 0.0f;

}

float AppTime::getDeltaTime() const
{
	return deltaTime;
}
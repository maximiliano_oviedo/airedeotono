#pragma once
#include "WindowsApp.h"

class MainApp : public WinApplication
{
public:
	MainApp(void);
	virtual ~MainApp(void);

private:
	virtual bool AppBegin();
	virtual bool AppUpdate();
	virtual bool AppRender();
	virtual bool AppEnd();
	virtual int WindowMainLoop();

	void ProcessInput();

	bool InitializeDirect3DX();

	HRESULT InitDirect3DXApp();

private:
	class Direct3DXCore* direct3DXCore;
	/*class WorldGrid *worldGrid;
	class SceneManager *sceneManager;*/
	class SquareGeometry* square;
	class AppTime *appTime;
};


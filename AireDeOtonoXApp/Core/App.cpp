#include "App.h"
#include "Direct3DX.h"
#include "AppTime.h"
#include "../Camera/MainCamera.h"
#include "../3DObjects/Square.h"

MainApp::MainApp(void) : WinApplication(),
square(nullptr)
//direct3DXCore(nullptr),
//worldGrid(nullptr),
//sceneManager(nullptr)
{ }

MainApp::~MainApp(void)
{
	if (direct3DXCore) { delete direct3DXCore; direct3DXCore = nullptr; }
	/*if (worldGrid) { worldGrid = nullptr; }
	if (sceneManager) { delete sceneManager; sceneManager = nullptr; }*/
}

bool MainApp::AppBegin()
{
	InitializeDirect3DX();

	appTime = new AppTime();
	appTime->Init();

	MainCamera* mainCamera = MainCamera::GetInstance();
	mVector<float> cameraPosition = mVector<float>(0, 20, -150.0f);
	mVector<float> cameraViewDirection = mVector<float>(0, 20, 0.1f);
	mainCamera->Initialize(cameraPosition, cameraViewDirection);

	square = SquareGeometry::GetInstance();
	square->Initialize(direct3DXCore->Get3DDevice(), direct3DXCore->GetDeviceContext());
	square->createModelGeometry();

	/*sceneManager = new SceneManager();
	sceneManager->Initialize(direct3DXCore->Get3DDevice(), direct3DXCore->GetDeviceContext());*/

	return true;
}

bool MainApp::InitializeDirect3DX()
{

	if (FAILED(InitDirect3DXApp()))
	{
		MessageBox(0, LPCSTR("Inicialización de Direct3DX - Fallida"),
			LPCSTR("Error Fatal"), MB_OK);
		return 0;
	}

	return 1;
}

HRESULT MainApp::InitDirect3DXApp()
{
	unsigned int w = 0, h = 0;
	GetWindowDims(w, h);

	direct3DXCore = new Direct3DXCore();
	return direct3DXCore->Initialize(GetAppHwnd(), w, h);
}

int MainApp::WindowMainLoop()
{
	MSG msg;
	ZeroMemory(&msg, sizeof(MSG));

	while (1)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				break;

			TranslateMessage(&msg);
			DispatchMessage(&msg);

		}
		else
		{
			//AppUpdate();
			//AppRender();
		}
	}

	AppEnd();
	return (int)msg.wParam;
}

bool MainApp::AppUpdate()
{
	ProcessInput();
	appTime->timeInstant();

	float deltaTime = 0.0f;
	deltaTime = appTime->getDeltaTime();

	/*sceneManager->update(deltaTime);*/
	return true;
}

void MainApp::ProcessInput()
{
	static UCHAR pKeyBuffer[256];
	if (!GetKeyboardState(pKeyBuffer)) return;

	const float sleept = 0.1f;
	static float ontime = 0.f;
	static bool iswait = false;
}

bool MainApp::AppRender()
{
	const FLOAT CLEAR_COLOR_RGBA[4] = { 0.1f, 0.1f, 0.1f, 1.f };
	direct3DXCore->clearColor(CLEAR_COLOR_RGBA);
	/*worldGrid->render();
	sceneManager->render();*/
	square->render();
	direct3DXCore->Present();

	return true;
}


bool MainApp::AppEnd()
{
	direct3DXCore->Close();
	/*worldGrid->release();
	sceneManager->release();*/
	return true;
}

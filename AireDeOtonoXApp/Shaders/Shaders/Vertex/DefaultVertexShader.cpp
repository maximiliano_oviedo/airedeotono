#include "DefaultVertexShader.h"
#include <d3d11.h>
#include <d3dCompiler.h>
#include "../Recursos/SafeRelease.h"
#include <string>

DefaultVertexShader::DefaultVertexShader() : IVertexShader()
{ }

DefaultVertexShader::~DefaultVertexShader()
{ }

const D3D11_INPUT_ELEMENT_DESC vertexLayoutDesc[] =
{
	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "COLOR", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
};

char DefaultVertexShader::getShaderId()
{
	return 001;
}

VertexShaderData* DefaultVertexShader::getVertexShader(ID3D11Device* device)
{
	VertexShaderData* vertexShaderData = new VertexShaderData();

	HRESULT hResult = S_OK;

	ID3DBlob* ShaderBlob;

	LPWSTR compileVertexShader = L"data/VertexShader_d.cso";

	hResult = D3DReadFileToBlob(compileVertexShader, &ShaderBlob);

	if (FAILED(hResult))
		return false;

	hResult = device->CreateVertexShader(ShaderBlob->GetBufferPointer(), ShaderBlob->GetBufferSize(), nullptr, &vertexShaderData->vertexShader);
	if (FAILED(hResult))
		return false;

	hResult = device->CreateInputLayout(vertexLayoutDesc, _countof(vertexLayoutDesc), ShaderBlob->GetBufferPointer(), ShaderBlob->GetBufferSize(), &vertexShaderData->inputLayout);
	if (hResult != S_OK)
		return false;
	
	SafeRelease(ShaderBlob);

	return vertexShaderData;
}
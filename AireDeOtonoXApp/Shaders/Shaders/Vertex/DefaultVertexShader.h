#pragma once
#include "../VertexShader.h"

class DefaultVertexShader : public IVertexShader
{
public:
	DefaultVertexShader();
	~DefaultVertexShader();

	VertexShaderData* getVertexShader(struct ID3D11Device* device);
	char getShaderId();

private:

};
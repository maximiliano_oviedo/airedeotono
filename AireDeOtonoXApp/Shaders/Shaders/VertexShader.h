#pragma once

struct VertexShaderData
{
public:
	VertexShaderData() : vertexShader(nullptr), inputLayout(nullptr) {};

	struct ID3D11VertexShader* vertexShader;
	struct ID3D11InputLayout* inputLayout;
};

class IVertexShader 
{
public:
	virtual VertexShaderData* getVertexShader(struct ID3D11Device* device) = 0;
	virtual char getShaderId() = 0;

	virtual ~IVertexShader()
	{ }

protected:
	IVertexShader() {}

	//struct ID3D11VertexShader* vertexShader;
	//struct ID3D11InputLayout *inputLayout;
};
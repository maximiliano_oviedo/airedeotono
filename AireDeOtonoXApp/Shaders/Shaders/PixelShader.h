#pragma once 

class IPixelShader
{
public:
	virtual struct ID3D11PixelShader* getPixelShader(struct ID3D11Device* device) = 0;

	virtual ~IPixelShader()
	{ }

protected:
	IPixelShader() {}
};
#include "DefaultPixelShader.h"
#include <d3d11.h>
#include <d3dCompiler.h>
#include "../Recursos/SafeRelease.h"


DefaultPixelShader::DefaultPixelShader() : IPixelShader()
{ }

DefaultPixelShader::~DefaultPixelShader()
{ }

ID3D11PixelShader* DefaultPixelShader::getPixelShader(struct ID3D11Device* device)
{
	ID3D11PixelShader* pixelShader = nullptr;

	HRESULT hResult = S_OK;

	ID3DBlob* ShaderBlob;

	LPWSTR compilePixelShader = L"data/PixelShaderColor_d.cso";

	hResult = D3DReadFileToBlob(compilePixelShader, &ShaderBlob);
	if (FAILED(hResult))
		return 0;

	hResult = device->CreatePixelShader(ShaderBlob->GetBufferPointer(), ShaderBlob->GetBufferSize(), nullptr, &pixelShader);
	if (FAILED(hResult))
		return 0;

	SafeRelease(ShaderBlob);

	return pixelShader;
}
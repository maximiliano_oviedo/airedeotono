#pragma once
#include "../PixelShader.h"

class DefaultPixelShader : public IPixelShader
{
public:
	DefaultPixelShader();
	~DefaultPixelShader();

	struct ID3D11PixelShader* getPixelShader(struct ID3D11Device* device);

};
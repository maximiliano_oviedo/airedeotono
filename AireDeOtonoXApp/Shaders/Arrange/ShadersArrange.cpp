#include "ShadersArrange.h"
#include "../Bodega/Bodega.h"
#include "../Factory/ShaderFactory.h"
//#include "../Policies/Shader.h"

ShadersArrange::ShadersArrange() : bodegaVertexShader(nullptr), bodegaPixelShader(nullptr)
{
	bodegaVertexShader = new Bodega<std::tuple<ID3D11VertexShader*, ID3D11InputLayout*>>();
	bodegaPixelShader = new Bodega<struct ID3D11PixelShader*>();
}

ShadersArrange::~ShadersArrange()
{ }

/*
bool ShadersArrange::arrangeShaderClass(ShaderClass, class IShader)
{
	ShaderFactory shaderFactory;
	shaderFactory.
	//Shader* shader = BodegaShader->getShaderClass(shaderClass);
	
	if (shader == nullptr)
	{	
		shader = shaderFactory->createShaderClass();
		BodegaShader->guardarShaderClass(shader);
	}

	/*ShaderManager<Shader, PixelShaderPolicy>* pixelShader = new ShaderManager<Shader, PixelShaderPolicy>();
	
	typedef ShaderPolicy<Shader> shaderp;
	
	typedef ShaderManager<Shader, PixelShaderPolicy>* shaderPixelPolicy;


	ShaderFactory fabrica;

	fabrica.getNewShader(shaderPixelPolicy);

	return true;
}
*/

bool ShadersArrange::arrangeVertexShaderClass(IVertexShader* vertexShader, ID3D11Device* device)
{
	ShaderFactory fabrica;	

	char vertexShaderId = fabrica.getVertexShaderName(vertexShader);

	std::tuple<ID3D11VertexShader*, ID3D11InputLayout*> vertexShaderTuple = bodegaVertexShader->getObjetoEnBodegaConLlave(vertexShaderId);

	if (!std::get<0>(vertexShaderTuple))
	{
		vertexShaderTuple = fabrica.getNewVertexShader(vertexShader, device);
		bodegaVertexShader->bodegaMap[vertexShaderId] = vertexShaderTuple;
		//bodegaVertexShader->setObjetoEnBodegaConLlave(vertexShaderId, &vertexShaderTuple);
	}

	return true;
}

bool ShadersArrange::arrangePixelShaderClass(IPixelShader* pixelShader, ID3D11Device* device)
{
	return true;
}
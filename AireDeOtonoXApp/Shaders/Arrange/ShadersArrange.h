#pragma once
//#include "../Bodega/Bodega.h"
#include <tuple>

struct ID3D11VertexShader;
struct ID3D11InputLayout;
template< class T > class Bodega;

class ShadersArrange
{
public:
	ShadersArrange();
	~ShadersArrange();

	bool arrangeVertexShaderClass(class IVertexShader*, struct ID3D11Device* device);
	bool arrangePixelShaderClass(class IPixelShader*, struct ID3D11Device* device);


private:
	Bodega<std::tuple<ID3D11VertexShader*, ID3D11InputLayout*>>* bodegaVertexShader;
	Bodega<struct ID3D11PixelShader*>* bodegaPixelShader;
	//Bodega<Shader<class Created>>* bodegaShaders;
};
#pragma once
#include <string>

class IShader
{
public:	
	virtual ~IShader()
	{ }

protected:
	IShader() : titulo("shader")
	{ }

protected:
	std::string titulo;
};

class DefVertexShader : public IShader
{
public:
	DefVertexShader();

	bool startShader(struct ID3D11Device* device);
};


/* Dise�o basado en varias politicas*/
/*class Shader
{
public:
	void startShader()
	{

	}
};

template <class T> struct VertexShaderPolicy
{
public:
	struct ID3D11VertexShader* vertexShader;
	struct ID3D11InputLayout *inputLayout;
};

template <class T> struct PixelShaderPolicy
{
public:
	struct ID3D11PixelShader* pixelShader;
};

template
< 
	class T,
	template <class> class ShaderPolicy 
> 
class ShaderManager : public ShaderPolicy<T>
{
public:

};
*/

// Ejemplo de una pol�tica
/*
template <class T> struct DibujadoRect
{

};

template <class T> struct DibujadoTriangulo
{

};

template <class T> struct DibujadoCirclulo
{

};

// Implementar una pol�tica

template<class DibujadoPolitica>
class ObjetoRender : public DibujadoPolitica
{

};

class Dibujo
{

};

typedef ObjetoRender<DibujadoTriangulo<Dibujo>> miPolitica;
miPolitica* estoEsUnNuevoTipo = new miPolitica();

miPolitica* estoEsUnaFuncionQueRegresaLaPolitica()
{

}
*/

template <class T> struct ShaderPInterfaz
{

};

template <class T> 
struct ShaderPVertex : public ShaderPInterfaz<IShader>
{

};

template<class T>
class ShaderPolitica : public T
{

};

ShaderPInterfaz<IShader>* algo;







template <class T> struct DibujadoTriangulo
{

};

template<class DibujadoPolitica>
class ObjetoRender : public DibujadoPolitica
{

};

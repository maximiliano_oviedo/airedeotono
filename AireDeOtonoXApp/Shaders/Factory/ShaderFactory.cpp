#include "ShaderFactory.h"
#include <d3dCompiler.h>
#include <d3d11.h>
#include <assert.h>
#include "../Recursos/SafeRelease.h"

#include "../Shaders/PixelShader.h"
#include "../Shaders/VertexShader.h"
#include "../Shaders/Vertex/DefaultVertexShader.h"
//#include "../Shaders/Policies/Shader.h"

ShaderFactory::ShaderFactory()
{

}

ShaderFactory::~ShaderFactory()
{

}

char ShaderFactory::getVertexShaderName(class IVertexShader* vertexShader)
{
	return vertexShader->getShaderId();
}

std::tuple<ID3D11VertexShader*, ID3D11InputLayout*> ShaderFactory::getNewVertexShader(IVertexShader* vertexShader, struct ID3D11Device* device)
{
	assert(device != nullptr);
	
	//Inicializar el vertex de acuerdo a la clase seleccionada
	vertexShader = new DefaultVertexShader();

	/*if (!vertexShader->startShader(device))
	{
		MessageBox(0, LPCSTR("Inicialización de Vertex Shader - Fallida"),
			LPCSTR("Error Fatal"), MB_OK);
		return 0;
	}*/

	VertexShaderData* vertexShaderData = vertexShader->getVertexShader(device);

	std::tuple<ID3D11VertexShader*, ID3D11InputLayout*> vertexShaderTuple = std::tuple<ID3D11VertexShader*, ID3D11InputLayout*>(vertexShaderData->vertexShader, vertexShaderData->inputLayout);
	
	return vertexShaderTuple;
}

ID3D11PixelShader* getNewPixelShader(IPixelShader* pixelShader, struct ID3D11Device* device)
{
	return nullptr;
}

/*SVertexShader* ShaderFactory::getVertexShader(ID3D11Device* device)
{
	SVertexShader* sVertexShader = new SVertexShader();

	HRESULT hResult = S_OK;

	ID3DBlob* ShaderBlob;

	LPWSTR compileVertexShader = L"data/VertexShader_d.cso";

	hResult = D3DReadFileToBlob(compileVertexShader, &ShaderBlob);
	if (FAILED(hResult))
		return 0;

	hResult = device->CreateVertexShader(ShaderBlob->GetBufferPointer(), ShaderBlob->GetBufferSize(), nullptr, &sVertexShader->vertexShader);
	if (FAILED(hResult))
		return 0;

	hResult = device->CreateInputLayout(vertexLayoutDesc, _countof(vertexLayoutDesc), ShaderBlob->GetBufferPointer(), ShaderBlob->GetBufferSize(), &sVertexShader->inputLayout);
	if (hResult != S_OK)
		return 0;

	SafeRelease(ShaderBlob);

	return sVertexShader;
}*/


/*ID3D11PixelShader* ShaderFactory::getPixelShader(struct ID3D11Device *device)
{
	ID3D11PixelShader* pixelShader = nullptr;

	HRESULT hResult = S_OK;

	ID3DBlob* ShaderBlob;

	LPWSTR compilePixelShader = L"data/PixelShaderColor_d.cso";

	hResult = D3DReadFileToBlob(compilePixelShader, &ShaderBlob);

	if (FAILED(hResult))
		return false;

	hResult = device->CreatePixelShader(ShaderBlob->GetBufferPointer(), ShaderBlob->GetBufferSize(), nullptr, &pixelShader);
	if (FAILED(hResult))
		return false;

	SafeRelease(ShaderBlob);

	return pixelShader;
}*/

/*bool ShaderFactory::getNewShader(ID3D11Device *device, ShaderPolicy)
{
	VertexShader* vertexShader = new VertexShader();
	vertexShader->startShader(device);
}*/
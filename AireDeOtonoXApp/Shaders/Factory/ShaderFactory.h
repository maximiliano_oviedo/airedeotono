#pragma once
#include <string>
#include <tuple>
//#include "../Policies/Shader.h"

/*struct SVertexShader 
{
public:
	SVertexShader() : vertexShader(nullptr), inputLayout(nullptr) {}
	struct ID3D11VertexShader* vertexShader;
	struct ID3D11InputLayout *inputLayout;
};

template <class> class ShaderPolicy;*/
template<class T> class Plantilla;
class IShader;
class ShaderFactory
{
public:
	ShaderFactory();
	~ShaderFactory();

	char getVertexShaderName(class IVertexShader* vertexShader);
	std::tuple<struct ID3D11VertexShader*, struct ID3D11InputLayout*> getNewVertexShader(class IVertexShader* vertexShader, struct ID3D11Device* device);
	struct ID3D11PixelShader* getNewPixelShader(class IPixelShader* pixelShader, struct ID3D11Device* device);
	
	//SVertexShader* getVertexShader(struct ID3D11Device* device);
	//struct ID3D11PixelShader* getPixelShader(struct ID3D11Device *device);
};
cbuffer PerApplication : register(b0)
{
	matrix projectionMatrix;
}

cbuffer PerFrame : register(b1)
{
	matrix viewMatrix;
}

cbuffer PerObject : register(b2)
{
	matrix modelMatrix;
}

struct VertexShaderInput
{
	float3 position : POSITION;
	float3 color: COLOR0;
	//float2 tex : TEXCOORD;
};

struct PixelShaderInput
{
	float4 position : SV_POSITION;
	//float2 tex : TEXCOORD;
	float4 color : COLOR0;
};


PixelShaderInput main(VertexShaderInput input)
{
	PixelShaderInput output;

	output.position = float4(input.position, 1.0f);

	// Hacer transformaciones.
	output.position = mul(output.position, modelMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);

	// Pasar el color.
	output.color = float4(input.color, 1.0f);

	// Pasar la textura
	//output.tex = input.tex;

	return output;
}
Texture2D txDiffuse : register(t0);
SamplerState samLinear : register(s0);

cbuffer psColor : register(c0)
{
	float4 constantColor;
}

struct PixelShaderInput
{
	float4 position : SV_POSITION;
	//float2 tex : TEXCOORD;
	float4 color : COLOR0;
};

float4 main(PixelShaderInput input) : SV_TARGET
{
	//return txDiffuse.Sample(samLinear, input.tex);
	return constantColor;
}



#pragma once
#include "ShaderStructures.h"

struct ID3D11Device;
struct ID3D11InputLayout;
struct ID3D11VertexShader;
struct ID3D11PixelShader;
struct ID3D11Buffer;

enum class PixelShaderType
{
	NORMALPIXEL,
	COLORPIXEL,
	TEXTUREPIXEL,
	COUNT
};

class ShaderCreator
{

public:
	ShaderCreator();
	virtual ~ShaderCreator();

	bool Initialize(PixelShaderType shaderType, const D3D11_INPUT_ELEMENT_DESC *vertexID, unsigned int deviceSize, ID3D11Device *device);

	ID3D11VertexShader *GetVertexShader()		{ return vertexShader; }
	ID3D11PixelShader *GetPixelShader()			{ return pixelShader; }

	ID3D11Buffer *GetAppConstantBuffer()		{ return shaderBuffers[SB_App]; }
	ID3D11Buffer *GetFrameConstantBuffer()		{ return shaderBuffers[SB_Frame]; }
	ID3D11Buffer *GetObjectConstantBuffer()		{ return shaderBuffers[SB_Object]; }

	ID3D11Buffer *GetColorConstantBuffer()		{ return pixelShaderColorBuffer; }
	sPSColor &GetColorBuffer()					{ return pixelShaderColorVector; }

	ID3D11Buffer** GetConstantBuffers()			{ return shaderBuffers; }

	ShaderCBuffer &GetProjectionBuffer()		{ return projection; }
	ShaderCBuffer &GetViewBuffer()				{ return view; }
	ShaderCBuffer &GetModelBuffer()				{ return model; }

	ID3D11InputLayout *GetInputLayout()			{ return inputLayout; }

private:

	bool initPixelShader(PixelShaderType shaderType, ID3D11Device *device);
	bool initVertexShader(const D3D11_INPUT_ELEMENT_DESC *vertexID, unsigned int deviceSize, ID3D11Device *device);

	bool createVertexShaderConstantBuffers(ID3D11Device *device);
	bool createPixelShaderConstantBuffer(ID3D11Device *device);

	void initBuffersStructs();

private:

	ID3D11Buffer		*shaderBuffers[NumShadersBuffers];
	ID3D11Buffer		*pixelShaderColorBuffer;

	ID3D11VertexShader		*vertexShader;		// Vertex shader
	ID3D11PixelShader		*pixelShader;		// Pixel shader

	ID3D11InputLayout		*inputLayout;

	ShaderCBuffer	projection;					// Buffer de la proyecci�n de la c�mara
	ShaderCBuffer	view;						// Buffer del view de la c�mara
	ShaderCBuffer	model;						// Buffer del modelo a renderizar

	sPSColor		pixelShaderColorVector;
};
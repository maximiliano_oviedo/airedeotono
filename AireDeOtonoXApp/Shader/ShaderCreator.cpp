
#include "ShaderCreator.h"
#include <assert.h>
#include <d3dCompiler.h>
#include "../Recursos/SafeRelease.h"

ShaderCreator::ShaderCreator() : inputLayout(nullptr),
vertexShader(nullptr),
pixelShader(nullptr)
{
	shaderBuffers[SB_App] = nullptr;
	shaderBuffers[SB_Frame] = nullptr;
	shaderBuffers[SB_Object] = nullptr;
	pixelShaderColorBuffer = nullptr;
}

ShaderCreator::~ShaderCreator()
{ }

bool ShaderCreator::Initialize(PixelShaderType shaderType, const D3D11_INPUT_ELEMENT_DESC *vertexID, unsigned int deviceSize, ID3D11Device *device)
{
	assert(device != nullptr);

	if (!initVertexShader(vertexID, deviceSize, device))
	{
		MessageBox(0, LPCSTR("Inicialización de Vertex Shader - Fallida"),
			LPCSTR("Error Fatal"), MB_OK);
		return 0;
	}

	if (!initPixelShader(shaderType, device))
	{
		MessageBox(0, LPCSTR("Inicialización de Pixel Shader - Fallida"),
			LPCSTR("Error Fatal"), MB_OK);
		return 0;
	}

	if (!createVertexShaderConstantBuffers(device))
	{
		MessageBox(0, LPCSTR("Error al crear los Vextex Shader constant buffer"),
			LPCSTR("Error Fatal"), MB_OK);
		return 0;
	}

	if (!createPixelShaderConstantBuffer(device))
	{
		MessageBox(0, LPCSTR("Error al crear los Pixel Shader constant buffer"),
			LPCSTR("Error Fatal"), MB_OK);
		return 0;
	}

	initBuffersStructs();

	return 1;
}

bool ShaderCreator::initVertexShader(const D3D11_INPUT_ELEMENT_DESC *vertexID, unsigned int deviceSize, ID3D11Device *device)
{
	HRESULT hResult = S_OK;

	ID3DBlob* ShaderBlob;

	LPWSTR compileVertexShader = L"data/VertexShader_d.cso";

	hResult = D3DReadFileToBlob(compileVertexShader, &ShaderBlob);
	if (FAILED(hResult))
		return 0;

	hResult = device->CreateVertexShader(ShaderBlob->GetBufferPointer(), ShaderBlob->GetBufferSize(), nullptr, &vertexShader);
	if (FAILED(hResult))
		return 0;

	hResult = device->CreateInputLayout(vertexID, deviceSize, ShaderBlob->GetBufferPointer(), ShaderBlob->GetBufferSize(), &inputLayout);
	if (hResult != S_OK)
		return 0;

	SafeRelease(ShaderBlob);

	return 1;
}

bool ShaderCreator::initPixelShader(PixelShaderType shaderType, ID3D11Device *device)
{
	HRESULT hResult = S_OK;

	ID3DBlob* ShaderBlob;

	LPWSTR compilePixelShader = L"data/PixelShaderColor_d.cso";

	if (shaderType == PixelShaderType::TEXTUREPIXEL)
		compilePixelShader = L"data/PixelShaderTexture_d.cso";

	hResult = D3DReadFileToBlob(compilePixelShader, &ShaderBlob);
	if (FAILED(hResult))
		return 0;

	hResult = device->CreatePixelShader(ShaderBlob->GetBufferPointer(), ShaderBlob->GetBufferSize(), nullptr, &pixelShader);
	if (FAILED(hResult))
		return 0;

	SafeRelease(ShaderBlob);

	return 1;
}

bool ShaderCreator::createVertexShaderConstantBuffers(ID3D11Device *device)
{
	D3D11_BUFFER_DESC constantBufferDesc = { 0 };
	ZeroMemory(&constantBufferDesc, sizeof(D3D11_BUFFER_DESC));

	constantBufferDesc.ByteWidth = sizeof(MathUtil::CMatrix);
	constantBufferDesc.CPUAccessFlags = 0;
	constantBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	constantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	constantBufferDesc.MiscFlags = 0;
	constantBufferDesc.StructureByteStride = 0;

	HRESULT hResult = device->CreateBuffer(&constantBufferDesc, 0, &shaderBuffers[SB_App]);
	if (FAILED(hResult))
		return 0;

	hResult = device->CreateBuffer(&constantBufferDesc, 0, &shaderBuffers[SB_Frame]);
	if (FAILED(hResult))
		return 0;

	hResult = device->CreateBuffer(&constantBufferDesc, 0, &shaderBuffers[SB_Object]);
	if (FAILED(hResult))
		return 0;

	return 1;
}

bool ShaderCreator::createPixelShaderConstantBuffer(ID3D11Device *device)
{
	D3D11_BUFFER_DESC constantBufferDesc = { 0 };
	ZeroMemory(&constantBufferDesc, sizeof(D3D11_BUFFER_DESC));

	constantBufferDesc.ByteWidth = sizeof(MathUtil::sVector4f);
	constantBufferDesc.CPUAccessFlags = 0;
	constantBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	constantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	constantBufferDesc.MiscFlags = 0;
	constantBufferDesc.StructureByteStride = 0;

	HRESULT hResult = device->CreateBuffer(&constantBufferDesc, nullptr, &pixelShaderColorBuffer);
	if (FAILED(hResult))
		return 0;

	return 1;
}

void ShaderCreator::initBuffersStructs()
{
	projection.cBuffer = MathUtil::MatrixIdentity();
	view.cBuffer = MathUtil::MatrixIdentity();
	model.cBuffer = MathUtil::MatrixIdentity();
	pixelShaderColorVector.color = MathUtil::GetWhiteColor();
}


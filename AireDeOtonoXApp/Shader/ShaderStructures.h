
#pragma once
#include <d3d11.h>
#include "../Recursos/UMath.h"

const D3D11_INPUT_ELEMENT_DESC basicVertexLayoutDesc[] =
{
	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
};

const D3D11_INPUT_ELEMENT_DESC vertexLayoutDesc[] =
{
	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "COLOR", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
};

const D3D11_INPUT_ELEMENT_DESC posVertexLayoutDesc[] =
{
	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
};

const D3D11_INPUT_ELEMENT_DESC cubistVertexLayoutDesc[] =
{
	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "SIZE", 0, DXGI_FORMAT_R32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
};

struct ShaderCBuffer
{
	MathUtil::CMatrix  cBuffer;
};

struct sPSColor
{
	MathUtil::sVector4f color;
};

enum ShadersBuffers
{
	SB_App,
	SB_Frame,
	SB_Object,
	NumShadersBuffers
};

struct BasicVertexColor
{
	float position[3];
	float color[3];
};

struct BasicTexVertex
{
	float position[3];
	float texture[2];
};

struct BasicColor
{
	float color[3];
};

struct BasicColorWithAlpha
{
	float color[4];
};

struct BasicPosVertex
{
	float position[3];
};



#include "stdafx.h"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace DirectXUnitTest
{
	#include "Direct3DX_11\Direct3DX.h"

	TEST_CLASS(DirectXAppTest)
	{
	public:
		Direct3DXCore* direct3DXCore;
		
		TEST_METHOD(TestDirectXInitialization)
		{
			direct3DXCore = new Direct3DXCore();
			HWND appWindow;
			HRESULT result = direct3DXCore->Initialize(appWindow, 1, 1);
			Assert::IsFalse(FAILED(result));

		}

	};
}